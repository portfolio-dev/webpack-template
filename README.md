## Webpack - набор настроек для фронт-приожений

Как установить:

1. Склонировать репозиторий **webpack-template.git**: `git clone https://bitbucket.org/businesstep/webpack-template.git`
2. Перейти в директорию проекта(для windows): `cd webpack-template`
3. Установить npm зависимости, выполнив команду: `npm install`

Как запустить:

- `npm run dev`	- для компиляции в режиме разработки 
- `npm run prod` 	- для компиляции в режиме прода
- `npm run watch` - для отслеживания изменений и автоматической перекомпиляции
- `npm run start` - для локального запуска на webpack-dev-server
